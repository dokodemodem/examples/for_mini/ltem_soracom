#pragma once
#include <dokodemo.h>

#define MAKER_CODE 'T'
#define COMMAND_1 'S'

struct SENSOR_DATA
{
    char header[2];
    uint8_t sqeuense;
    uint8_t sum;
    uint8_t ei;
    float sensor;
    uint8_t gpio;
} __attribute__((__packed__));

uint8_t calcsum(uint8_t data[], int size);
