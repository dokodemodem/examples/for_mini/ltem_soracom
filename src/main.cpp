/*
 * Sample program for DokodeModem
 * inside MQTT pub/sub sample for AWS IoT Core.
 * Copyright (c) 2024 Circuit Desgin,Inc
 * Released under the MIT license
 */
#include <dokodemo.h>
#include <nRF9160Modem.h>
#include <nRF9160ModemClient.h>
#include "modem_setting.h"
#include "data.h"

#define SEND_CYCLE_MS 60000

DOKODEMO Dm = DOKODEMO();
nRF9160Modem modem = nRF9160Modem();
nRF9160ModemClient client(&modem);

static const char *host = "funnel.soracom.io";
static int port = 23080;

bool modem_init(bool enableSSL)
{
  SerialDebug.println("\r\n### initialize LTE-M");

  Dm.ModemPowerCtrl(ON); // モデムの電源を入れます

  if (!modem.init(UartModem)) // その直後に初期化してください
  {
    SerialDebug.println("### init Error");
    return false;
  }

  // 携帯網につなげます
  SerialDebug.println("### Start activate...");
  if (!modem.activate(APN, USERNAME, PASSWORD))
  {
    SerialDebug.println("### Error");
    return false;
  }
  SerialDebug.println("### OK");

  // 機器のIPアドレスを表示します
  std::string ipadd;
  modem.getIPaddress(&ipadd);
  SerialDebug.print("IPaddress: ");
  SerialDebug.println(ipadd.c_str());

  // 機器のUUIDアドレスを表示します
  std::string add;
  modem.getUUID(&add);
  SerialDebug.print("UUID: ");
  SerialDebug.println(add.c_str());

  // 機器のファームウェアバージョンを表示します
  modem.getVersion(&add);
  SerialDebug.print("Version: ");
  SerialDebug.println(add.c_str());

  // LTE-Mの時間をRTCに設定します
  std::tm res = {};
  if (modem.getlocaltime(&res))
  {
    DateTime tm = DateTime(res.tm_year, res.tm_mon + 1, res.tm_mday, res.tm_hour, res.tm_min, res.tm_sec);
    Dm.rtcAdjust(tm);
  }

  SerialDebug.println("### OK");
  return true;
}

uint32_t _elaspTime = 0;
uint8_t _sqeuense = 0;
uint8_t _errcount = 3;

void setup()
{
  delay(2000); // デバッグ出力のため、USBが認識するまで待ちます。

  Dm.begin(); // 初期化が必要です。

  SerialDebug.begin(115200); // デバッグ用
  UartModem.begin(115200);   // モデム用

  // 初期化が失敗した時のためリトライします
  int retry = 3;
  while (retry > 0)
  {
    if (modem_init(true))
      break;

    retry--;

    Dm.ModemPowerCtrl(OFF); // 電源切ってリトライします
    delay(100);
  }

  Dm.LedCtrl(GREEN_LED, OFF);
  Dm.LedCtrl(RED_LED, OFF);
  _elaspTime = millis() - SEND_CYCLE_MS;
}

void loop()
{
  uint32_t tmpTime = millis();
  if ((tmpTime - _elaspTime) >= SEND_CYCLE_MS)
  {
    _elaspTime = tmpTime;

    //適当にパケットを作成します
    SENSOR_DATA packet;
    packet.header[0] = MAKER_CODE;
    packet.header[1] = COMMAND_1;
    packet.ei = 0xaa;

    float volt = 0;
    for (int i = 0; i < 3; i++)
    {
      volt += Dm.readExADC();
    }
    volt /= 3;

    packet.sensor = volt;
    packet.sqeuense = _sqeuense++;
    packet.gpio = Dm.readExIN();
    packet.sum = 0;
    packet.sum = calcsum((uint8_t *)&packet, sizeof(packet));

    SerialDebug.print("\n### start connection ");
    SerialDebug.print(host);
    SerialDebug.println(":" + String(port));

    if (!client.connect(host, port))
    {
      SerialDebug.println("### connect error");
      _errcount--;
      if (_errcount == 0)
      {
        SerialDebug.println("### reboot");
        delay(1000);
        Dm.reset();
      }
      return;
    }

    client.write((uint8_t *)&packet, sizeof(packet));
    client.stop();
    SerialDebug.println("### send done");
  }
}
