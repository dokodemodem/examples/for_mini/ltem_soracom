#include "data.h"

//全部計算したら0xffになるサム値を返す
uint8_t calcsum(uint8_t data[], int size)
{
    uint8_t tmpsum = 0;

    for (int i = 0; i < size; i++)
    {
        tmpsum += data[i];
    }

    return ~tmpsum;
}
